# **README ProFlutterLogin Con Serenity **
Proyecto de automatizacion mobiles con screenplay y appium de app desarrollada con flutter

***

Requisitos del proyecto:
* [Gradle]: version 7.4 
* [JDk]: version 11
* [Serenity]: version 2.1.4
* [Appium_java_client]: version 7.5.1
* [Devco automation]: version 2.0.2
* [Appium server]: version 1.22.3 ->Xpath2.0

## **Run tests Chrome gradle:**
```
./gradle clean test 
./gradle test --tests "****" 
```
