package org.example.userinterfaces.pragma;

import co.com.devco.automation.mobile.locator.Locator;
import net.serenitybdd.screenplay.targets.Target;

import static co.com.devco.automation.mobile.locator.ElementFinder.theElementBy;
import static co.com.devco.automation.mobile.locator.Locator.locator;

public class DropdownButtonPage {
    public static final Locator ELEMENT_ONE_LIST_LOCATOR = locator().withAndroidAccesibilityId("Element One").withIosAccesibilityId("1");
    public static final Target ELEMENT_ONE_LIST = Target.the("Desplegar lista").located(theElementBy(ELEMENT_ONE_LIST_LOCATOR));
    public static final Locator ELEMENT_THREE_LIST_LOCATOR = locator().withAndroidAccesibilityId("Element Three").withIosAccesibilityId("1");
    public static final Target ELEMENT_THREE_LIST = Target.the("Seleccionar elemento tres de la lista").located(theElementBy(ELEMENT_THREE_LIST_LOCATOR));
}
