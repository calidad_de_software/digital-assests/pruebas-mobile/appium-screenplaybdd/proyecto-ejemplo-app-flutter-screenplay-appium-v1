package org.example.userinterfaces.pragma;

import co.com.devco.automation.mobile.locator.Locator;
import net.serenitybdd.screenplay.targets.Target;

import static co.com.devco.automation.mobile.locator.ElementFinder.theElementBy;
import static co.com.devco.automation.mobile.locator.Locator.locator;

public class ButtonsDemoPage {
    public static final Locator TITULO_BUTTONS_DEMO_LOCATOR = locator().withAndroidAccesibilityId("Show Case Material ").withIosAccesibilityId("1");
    public static final Target TITULO_BUTTONS_DEMO = Target.the("Titulo demo botones").located(theElementBy(TITULO_BUTTONS_DEMO_LOCATOR));
    public static final Locator TITULO_2_BUTTONS_DEMO_LOCATOR = locator().withAndroidAccesibilityId("Show Case Material ").withIosAccesibilityId("1");
    public static final Target TITULO_2_BUTTONS_DEMO = Target.the("Titulo demo botones").located(theElementBy(TITULO_2_BUTTONS_DEMO_LOCATOR));
}
