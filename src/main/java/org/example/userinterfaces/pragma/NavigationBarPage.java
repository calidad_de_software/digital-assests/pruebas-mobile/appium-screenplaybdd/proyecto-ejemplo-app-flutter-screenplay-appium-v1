package org.example.userinterfaces.pragma;

import co.com.devco.automation.mobile.locator.Locator;
import net.serenitybdd.screenplay.targets.Target;

import static co.com.devco.automation.mobile.locator.ElementFinder.theElementBy;
import static co.com.devco.automation.mobile.locator.Locator.locator;

public class NavigationBarPage {
    public static final Locator TAB_BUSINESS_LOCATOR = locator().withAndroidAccesibilityId("Business\n" +
            "Tab 2 of 3").withIosAccesibilityId("1");
    public static final Target TAB_BUSINESS = Target.the("Seleccionar tab business").located(theElementBy(TAB_BUSINESS_LOCATOR));
    public static final Locator TAB_SCHOOL_LOCATOR = locator().withAndroidAccesibilityId("School\n" +
           "Tab 3 of 3").withIosAccesibilityId("1");
    public static final Target TAB_SCHOOL = Target.the("Seleccionar tab School").located(theElementBy(TAB_SCHOOL_LOCATOR));
    public static final Locator TXT_SCHOOL_LOCATOR = locator().withAndroidAccesibilityId(" School").withIosAccesibilityId("1");
    public static final Target TXT_SCHOOL = Target.the("Seleccionar tab School").located(theElementBy(TXT_SCHOOL_LOCATOR));
}
