package org.example.userinterfaces.pragma;

import co.com.devco.automation.mobile.locator.Locator;
import net.serenitybdd.screenplay.targets.Target;

import static co.com.devco.automation.mobile.locator.ElementFinder.theElementBy;
import static co.com.devco.automation.mobile.locator.Locator.locator;

public class SwitchScreenPage {
    public static final Locator SWITCH_LOCATOR = locator().withAndroidXpathStatic("//android.view.View[@content-desc='Switch'][2]/following-sibling::android.widget.Switch[1]").withIosAccesibilityId("1");
    public static final Target SWITCH = Target.the("presionar switch").located(theElementBy(SWITCH_LOCATOR));
    public static final Locator SWITCH_TILE_LOCATOR = locator().withAndroidXpathStatic("//android.view.View[@content-desc='Switch'][2]/following-sibling::android.widget.Switch[2]").withIosAccesibilityId("1");
    public static final Target SWITCH_TILE = Target.the("presionar switch tile").located(theElementBy(SWITCH_TILE_LOCATOR));
    public static final Locator TXT_SWITCH_TILE_LOCATOR = locator().withAndroidXpathStatic("//android.widget.Switch[@content-desc='Habilitar Switch']/following-sibling::android.view.View").withIosAccesibilityId("1");
    public static final Target TXT_SWITCH_TILE = Target.the("Texto al activar switch").located(theElementBy(TXT_SWITCH_TILE_LOCATOR));
}
