package org.example.userinterfaces.pragma;

import co.com.devco.automation.mobile.locator.Locator;
import net.serenitybdd.screenplay.targets.Target;

import static co.com.devco.automation.mobile.locator.ElementFinder.theElementBy;
import static co.com.devco.automation.mobile.locator.Locator.locator;

public class PrincipalPage {

    public static final Locator CARD_BUTTONS_DEMO_LOCATOR = locator().withAndroidAccesibilityId("Buttons Demo").withIosAccesibilityId("1");
    public static final Target CARD_BUTTONS_DEMO = Target.the("Seleccionar demo botones").located(theElementBy(CARD_BUTTONS_DEMO_LOCATOR));

    //CONTADOR
    public static final Locator CARD_CONTADOR_LOCATOR = locator().withAndroidAccesibilityId("App Contador").withIosAccesibilityId("1");
    public static final Target CARD_CONTADOR = Target.the("contador").located(theElementBy(CARD_CONTADOR_LOCATOR));

    public static final Locator BOTON_ATRAS_LOCATOR = locator().withAndroidXpathStatic("//*[@class='android.view.View'][1]/android.widget.Button").withIosAccesibilityId("1");
    public static final Target BOTON_ATRAS = Target.the("contador").located(theElementBy(BOTON_ATRAS_LOCATOR));

    // ALERTAS
    public static final Locator CARD_DIALOGS_DEMO_LOCATOR = locator().withAndroidAccesibilityId("Dialogs Demo").withIosAccesibilityId("1");
    public static final Target CARD_DIALOGS_DEMO = Target.the("Seleccionar  dialogs demo botones").located(theElementBy(CARD_DIALOGS_DEMO_LOCATOR));

    //cards
    public static final Locator BOTON_CARDS_DEMO_LOCATOR = locator().withAndroidAccesibilityId("Cards Demo").withIosAccesibilityId("1");
    public static final Target BOTON_CARDS_DEMO = Target.the("Abrir card demo").located(theElementBy(BOTON_CARDS_DEMO_LOCATOR));


    //datePicker
    public static final Locator CARD_DATEPICKER_LOCATOR = locator().withAndroidAccesibilityId("DatePicker screeen").withIosAccesibilityId("1");
    public static final Target CARD_DATEPICKER = Target.the("Seleccionar  dialogs demo botones").located(theElementBy(CARD_DATEPICKER_LOCATOR));


    public static final Locator BTN_VERSION_LOCATOR = locator().withAndroidAccesibilityId("v2").withIosAccesibilityId("1");
    public static final Target BTN_VERSION = Target.the("Cambiar version de la vista").located(theElementBy(BTN_VERSION_LOCATOR));

    //#################
    public static final Locator CARD_SWITCH_LOCATOR = locator().withAndroidAccesibilityId("Switch screeen").withIosAccesibilityId("1");
    public static final Target CARD_SWITCH = Target.the("Seleccionar demo botones").located(theElementBy(CARD_SWITCH_LOCATOR));


    //################drop down list
    public static final Locator CARD_DROPDOWN_LOCATOR = locator().withAndroidAccesibilityId("Dropdown Button").withIosAccesibilityId("1");
    public static final Target CARD_DROPDOWN = Target.the("Card dropDown list").located(theElementBy(CARD_DROPDOWN_LOCATOR));

    //################# inputs


    //################NAvigation bar
    public static final Locator CARD_NAVIGATION_BAR_LOCATOR = locator().withAndroidAccesibilityId("Navigation Bar Screen").withIosAccesibilityId("1");
    public static final Target CARD_NAVIGATION_BAR = Target.the("Seleccionar demo botones").located(theElementBy(CARD_NAVIGATION_BAR_LOCATOR));

    //#############################CArousel



    private PrincipalPage() {
        throw new IllegalStateException("user interface class");
    }
}
