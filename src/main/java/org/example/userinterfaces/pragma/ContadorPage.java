package org.example.userinterfaces.pragma;

import co.com.devco.automation.mobile.locator.Locator;
import net.serenitybdd.screenplay.targets.Target;

import static co.com.devco.automation.mobile.locator.ElementFinder.theElementBy;
import static co.com.devco.automation.mobile.locator.Locator.locator;

public class ContadorPage {
    public static final Locator BOTON_CONTADOR_LOCATOR = locator().withAndroidXpathStatic("//*[@class='android.widget.Button'][2]").withIosAccesibilityId("1");
    public static final Target BOTON_CONTADOR = Target.the("contador").located(theElementBy(BOTON_CONTADOR_LOCATOR));
    public static final Locator VALOR_CONTADOR_LOCATOR = locator().withAndroidXpathStatic("//android.view.View[@content-desc=\"You have pushed the button this many times:\"]/following-sibling::android.view.View[1]").withIosAccesibilityId("1");
    public static final Target VALOR_CONTADOR = Target.the("valor en pantalla").located(theElementBy(VALOR_CONTADOR_LOCATOR));
}
