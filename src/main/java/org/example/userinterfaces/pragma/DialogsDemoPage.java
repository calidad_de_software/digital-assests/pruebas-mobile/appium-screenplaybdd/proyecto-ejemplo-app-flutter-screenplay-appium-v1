package org.example.userinterfaces.pragma;

import co.com.devco.automation.mobile.locator.Locator;
import net.serenitybdd.screenplay.targets.Target;

import static co.com.devco.automation.mobile.locator.ElementFinder.theElementBy;
import static co.com.devco.automation.mobile.locator.Locator.locator;

public class DialogsDemoPage {
    public static final Locator BOTON_SHOW_ALERT_LOCATOR = locator().withAndroidAccesibilityId("Show AlertDialog Widget").withIosAccesibilityId("1");
    public static final Target BOTON_SHOW_ALERT = Target.the("Abrir alerta").located(theElementBy(BOTON_SHOW_ALERT_LOCATOR));
    public static final Locator TXT_ALERT_LOCATOR = locator().withAndroidAccesibilityId("This is a short description for the popup alert").withIosAccesibilityId("1");
    public static final Target TXT_ALERT = Target.the("Mensaje en la alerta").located(theElementBy(TXT_ALERT_LOCATOR));
    public static final Locator BOTON_OK_LOCATOR = locator().withAndroidAccesibilityId("OK").withIosAccesibilityId("1");
    public static final Target BOTON_OK = Target.the("Cierra alerta con ok").located(theElementBy(BOTON_OK_LOCATOR));
}
