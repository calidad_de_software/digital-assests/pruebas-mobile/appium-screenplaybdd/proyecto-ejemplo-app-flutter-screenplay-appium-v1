package org.example.questions.datapicker;

import net.serenitybdd.screenplay.Question;

import static org.example.hooks.Hooks.pragma;
import static org.example.userinterfaces.pragma.DatePickerPage.INPUT_FECHA;

public class DatePickerFactoryQuestion {

    public static Question<String> fechaIngresada(){
        return actor -> (
                INPUT_FECHA.resolveFor(pragma).getText()
        );
    }
}
