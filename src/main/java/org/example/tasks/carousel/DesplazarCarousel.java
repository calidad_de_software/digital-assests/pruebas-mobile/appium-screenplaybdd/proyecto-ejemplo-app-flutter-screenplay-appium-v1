package org.example.tasks.carousel;

import co.com.devco.automation.mobile.actions.Drag;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;

import static co.com.devco.automation.mobile.actions.DragDirection.LEFT;
import static co.com.devco.automation.mobile.actions.DragDirection.RIGHT;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static org.example.userinterfaces.pragma.CarouselPage.*;

public class DesplazarCarousel implements Task {


    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Drag.theElement(IMG).to(LEFT),
                Drag.theElement(IMG_DOS).to(LEFT),
                Drag.theElement(IMG_DOS).to(RIGHT)
        );
    }

    public static Performable dePageView() {
        return instrumented(DesplazarCarousel.class);
    }

}
