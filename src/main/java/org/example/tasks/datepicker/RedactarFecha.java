package org.example.tasks.datepicker;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Clear;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.example.userinterfaces.pragma.DatePickerPage;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class RedactarFecha implements Task {

    private final String fecha;

    public RedactarFecha(String fecha) {
        this.fecha = fecha;
    }

    public static Performable enCampo(String fecha) {
        return instrumented(RedactarFecha.class,fecha);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Click.on(DatePickerPage.INPUT_FECHA),
                WaitUntil.the(DatePickerPage.BTN_EDITAR_FECHA, isVisible()).forNoMoreThan(5).seconds(),
                Click.on(DatePickerPage.BTN_EDITAR_FECHA),
                Clear.field(DatePickerPage.INPUT_EDITAR_FECHA),
                Enter.theValue(fecha).into(DatePickerPage.INPUT_EDITAR_FECHA),
                Click.on(DatePickerPage.BTN_OK_FECHA)
        );
    }

}
