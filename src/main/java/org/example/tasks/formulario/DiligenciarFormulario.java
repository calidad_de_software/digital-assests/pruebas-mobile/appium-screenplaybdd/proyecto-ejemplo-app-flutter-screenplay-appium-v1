package org.example.tasks.formulario;

import co.com.devco.automation.mobile.abilities.Hide;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.ensure.Ensure;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static org.example.userinterfaces.pragma.FormularioPage.*;

public class DiligenciarFormulario implements Task {


    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Click.on(INPUT_ONE),
                Enter.theValue("a").into(INPUT_ONE),
                Click.on(AUTOCOMPLETAR_NOMBRE),
                Click.on(BOTON_VALIDAR),
                WaitUntil.the(VALIDACION_CAMPO, isVisible()).forNoMoreThan(5).seconds()
        );

        actor.attemptsTo(Ensure.that(VALIDACION_CAMPO.resolveFor(actor).getAttribute("content-desc")).contains("field is empty"));

        actor.attemptsTo(Click.on(INPUT_TWO),
                Enter.theValue("in flutter").into(INPUT_TWO),
                Click.on(BOTON_VALIDAR)
        );

        actor.attemptsTo(
                Hide.theKeyboard(),
                Click.on(CAMPO_NOMBRE),
                Enter.theValue("Pragma").into(CAMPO_NOMBRE),
                Hide.theKeyboard(),
                //Scroll.untilVisibleTarget(CAMPO_CELULAR).toBottom().untilMaxAttempts(5),
                Click.on(CAMPO_CELULAR),
                Enter.theValue("3201234567").into(CAMPO_CELULAR),
                Hide.theKeyboard(),
                Click.on(CAMPO_CLAVE),
                Hide.theKeyboard(),
                Enter.theValue("1245").into(CAMPO_CLAVE),
                Hide.theKeyboard(),
//                Scroll.untilVisibleTarget(BOTON_VALIDAR_DOS).toBottom().untilMaxAttempts(5),
                Click.on(VER_OCULTAR_CLAVE),
                Click.on(BOTON_VALIDAR_DOS),
                Hide.theKeyboard(),
                WaitUntil.the(MENSAJE_FORMULARIO_VALIDO, isVisible()).forNoMoreThan(3).seconds()
        );

        actor.attemptsTo(Ensure.that(MENSAJE_FORMULARIO_VALIDO.resolveFor(actor).getAttribute("content-desc")).contains("Form is valid"));
    }

    public static Performable deFlutter() {
        return instrumented(DiligenciarFormulario.class);
    }

}
