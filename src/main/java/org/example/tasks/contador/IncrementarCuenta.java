package org.example.tasks.contador;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isEnabled;
import static org.example.userinterfaces.pragma.ContadorPage.BOTON_CONTADOR;

public class IncrementarCuenta implements Task {

    private final int times;

    public IncrementarCuenta(int times) {
        this.times = times;
    }

    public static  IncrementarCuenta numVeces(int times){
        return new IncrementarCuenta(times);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                WaitUntil.the(BOTON_CONTADOR, isEnabled()).forNoMoreThan(5).seconds()
        );
        for (int i=0; i<times;i++) {
            actor.attemptsTo(
                    Click.on(BOTON_CONTADOR)
            );
        }
    }
}
