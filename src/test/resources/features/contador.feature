# language: es
Característica: Contador
  Como un usuario de de Material Flutter App, quiero interactuar con elementos de App Contador

  @Carousel
  Escenario: Incrementar cuenta del contador
    Dado el usuario esta en App Contador
    Cuando el usuario incrementa la cuenta 4 veces
    Entonces el usuario deberia ver la cuenta en 4