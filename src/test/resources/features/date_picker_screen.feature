# language: es
Característica: Datepicker
  Como un usuario de de Material Flutter App, quiero interactuar con elementos datepicker

  Escenario: Redacción de fecha en datepicker
    Dado el usuario esta en Date Picker Screen
    Cuando el usuario redacta la fecha 12/24/2022 en el calendario
    Entonces el usuario deberia ver la fecha 2022-12-24