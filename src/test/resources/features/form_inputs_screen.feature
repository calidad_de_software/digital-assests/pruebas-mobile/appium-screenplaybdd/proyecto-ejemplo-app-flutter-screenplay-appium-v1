# language: es
Característica: Formulario Inputs Screen
  Como un usuario de Material Flutter App, quiero diligenciar el formulario de Inputs Screen

  @Formulario
  Escenario: Diligenciar el formulario
    Dado que el usuario esta en Inputs Screen
    Cuando diligencia el formulario
    Entonces el usuario deberia ver el mensaje Form is valid