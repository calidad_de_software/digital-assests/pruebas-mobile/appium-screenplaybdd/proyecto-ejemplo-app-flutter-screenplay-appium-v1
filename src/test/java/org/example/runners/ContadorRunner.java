package org.example.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/contador.feature",
        glue = {"org.example.stepdefinitions","org.example.hooks"},
//        tags = "@...",
        snippets = SnippetType.CAMELCASE
)
public class ContadorRunner {
}
