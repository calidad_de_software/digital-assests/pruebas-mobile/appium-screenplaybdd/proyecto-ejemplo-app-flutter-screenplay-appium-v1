package org.example.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/form_inputs_screen.feature",
        glue = {"org.example.stepdefinitions","org.example.hooks"},
//        tags = "@Formulario",
        snippets = SnippetType.CAMELCASE
)
public class FormInputsScreenRunner {
}
