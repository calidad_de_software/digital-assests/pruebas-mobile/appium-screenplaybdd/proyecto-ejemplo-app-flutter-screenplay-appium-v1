package org.example.stepdefinitions;

import co.com.devco.automation.mobile.actions.Scroll;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.example.questions.ObtenerTextoSegunCampoFlutter;
import org.example.tasks.contador.IncrementarCuenta;
import org.example.userinterfaces.pragma.ContadorPage;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static org.example.hooks.Hooks.pragma;
import static org.example.userinterfaces.pragma.PrincipalPage.*;
import static org.hamcrest.CoreMatchers.equalTo;

public class ContadorStepDefinitions {

    @Dado("^el usuario esta en App Contador$")
    public void elUsuarioEstaEnAppContador() {
        pragma.wasAbleTo(
                WaitUntil.the(CARD_BUTTONS_DEMO, isVisible()).forNoMoreThan(5).seconds(),
                Scroll.untilVisibleTarget(CARD_CONTADOR).toBottom().untilMaxAttempts(5),
                Click.on(CARD_CONTADOR)
        );
    }

    @Cuando("^el usuario incrementa la cuenta (\\d+) veces$")
    public void elUsuarioIncrementaLaCuentaVez(int times) {
        pragma.attemptsTo(IncrementarCuenta.numVeces(times));
    }

    @Entonces("^el usuario deberia ver la cuenta en (\\d+)$")
    public void elUsuarioDeberiaVerLaCuentaEn(int times) {
        pragma.should(
                seeThat("La cuenta actual", ObtenerTextoSegunCampoFlutter.presentaElMensaje(ContadorPage.VALOR_CONTADOR),equalTo(Integer.toString(times)))
        );
    }
}
