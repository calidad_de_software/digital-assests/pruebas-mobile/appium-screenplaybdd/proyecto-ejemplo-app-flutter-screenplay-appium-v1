package org.example.stepdefinitions;

import co.com.devco.automation.mobile.actions.Scroll;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.example.questions.ObtenerTextoSegunCampoFlutter;
import org.example.userinterfaces.pragma.SwitchScreenPage;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static org.example.hooks.Hooks.pragma;
import static org.example.userinterfaces.pragma.PrincipalPage.*;
import static org.hamcrest.CoreMatchers.equalTo;

public class SwitchScreenStepDefinitions {

    @Dado("^el usuario esta en Switch Screen$")
    public void elUsuarioEstaEnSwitchScreen() {
        pragma.wasAbleTo(
                Scroll.untilVisibleTarget(CARD_SWITCH).toBottom().untilMaxAttempts(5),
                Click.on(CARD_SWITCH)
        );
    }

    @Cuando("^el usuario switchea el Switch Tile$")
    public void elUsuarioSwitcheaElSwitchTile() {
        pragma.attemptsTo(
                WaitUntil.the(SwitchScreenPage.SWITCH, isVisible()).forNoMoreThan(5).seconds(),
                Click.on(SwitchScreenPage.SWITCH_TILE)
        );
    }

    @Entonces("^deberia ver (.*)$")
    public void elUsuarioDeberiaVerTheValueIsTrue(String statusValue) {
        pragma.should(
                seeThat(ObtenerTextoSegunCampoFlutter.presentaElMensaje(SwitchScreenPage.TXT_SWITCH_TILE), equalTo(statusValue))
        );
    }

}
