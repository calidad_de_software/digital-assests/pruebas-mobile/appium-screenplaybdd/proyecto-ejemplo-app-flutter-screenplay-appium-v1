package org.example.stepdefinitions;

import co.com.devco.automation.mobile.actions.Scroll;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.example.questions.ObtenerTextoSegunCampoFlutter;
import org.example.userinterfaces.pragma.DialogsDemoPage;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isEnabled;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static org.example.hooks.Hooks.pragma;
import static org.example.userinterfaces.pragma.PrincipalPage.*;
import static org.example.userinterfaces.pragma.DialogsDemoPage.BOTON_SHOW_ALERT;
import static org.hamcrest.CoreMatchers.equalTo;

public class DialogsDemoStepDefinitions {

    @Dado("^el usuario esta en Dialogs Demo$")
    public void elUsuarioEstaEnDialogsDemo() {
        pragma.wasAbleTo(
                WaitUntil.the(CARD_BUTTONS_DEMO, isVisible()).forNoMoreThan(5).seconds(),
                Scroll.untilVisibleTarget(CARD_DIALOGS_DEMO).toBottom().untilMaxAttempts(5),
                Click.on(CARD_DIALOGS_DEMO)
        );
    }

    @Cuando("^el usuario muestra la alerta$")
    public void elUsuarioMuestraLaAlerta() {
        pragma.attemptsTo(
                WaitUntil.the(BOTON_SHOW_ALERT, isEnabled()).forNoMoreThan(5).seconds(),
                Click.on(BOTON_SHOW_ALERT)
        );
    }

    @Entonces("^el usuario deberia ver la alerta$")
    public void elUsuarioDeberiaVerLaAlerta() {
        pragma.should(
                seeThat(ObtenerTextoSegunCampoFlutter.presentaElMensaje(DialogsDemoPage.TXT_ALERT),equalTo("This is a short description for the popup alert"))
        );
    }

}
