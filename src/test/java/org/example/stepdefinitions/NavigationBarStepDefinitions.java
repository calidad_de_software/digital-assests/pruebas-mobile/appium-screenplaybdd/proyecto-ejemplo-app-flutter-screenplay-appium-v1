package org.example.stepdefinitions;

import co.com.devco.automation.mobile.actions.Scroll;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.example.questions.ObtenerTextoSegunCampoFlutter;
import org.example.userinterfaces.pragma.NavigationBarPage;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static org.example.hooks.Hooks.pragma;
import static org.example.userinterfaces.pragma.PrincipalPage.*;
import static org.hamcrest.CoreMatchers.equalTo;

public class NavigationBarStepDefinitions {
    @Dado("^el usuario esta en Navigation Bar Screen$")
    public void elUsuarioEstaEnNavigationBarScreen() {
        pragma.wasAbleTo(
                Scroll.untilVisibleTarget(CARD_NAVIGATION_BAR).toBottom().untilMaxAttempts(5),
                Click.on(CARD_NAVIGATION_BAR)
        );
    }

    @Cuando("^el usuario navega a School$")
    public void elUsuarioNavegaASchool() {
        pragma.attemptsTo(
                WaitUntil.the(NavigationBarPage.TAB_SCHOOL, isVisible()).forNoMoreThan(5).seconds(),
                Click.on(NavigationBarPage.TAB_SCHOOL)
        );
    }

    @Entonces("^el usuario deberia ver School$")
    public void elUsuarioDeberiaVerSchool() {
        pragma.should(
                seeThat(ObtenerTextoSegunCampoFlutter.presentaElMensaje(NavigationBarPage.TXT_SCHOOL), equalTo(" School"))
        );
    }
}
