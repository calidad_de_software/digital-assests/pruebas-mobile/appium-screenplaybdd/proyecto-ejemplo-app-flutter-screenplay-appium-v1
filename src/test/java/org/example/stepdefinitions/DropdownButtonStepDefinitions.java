package org.example.stepdefinitions;

import co.com.devco.automation.mobile.actions.Scroll;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.actions.Click;
import org.example.questions.ObtenerTextoSegunCampoFlutter;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.example.hooks.Hooks.pragma;
import static org.example.userinterfaces.pragma.DropdownButtonPage.ELEMENT_ONE_LIST;
import static org.example.userinterfaces.pragma.DropdownButtonPage.ELEMENT_THREE_LIST;
import static org.example.userinterfaces.pragma.PrincipalPage.CARD_DROPDOWN;
import static org.hamcrest.CoreMatchers.equalTo;

public class DropdownButtonStepDefinitions {
    @Dado("^el usuario esta en Dropdown Button$")
    public void elUsuarioEstaEnDropdownButton() {
        pragma.wasAbleTo(
                Scroll.untilVisibleTarget(CARD_DROPDOWN).toTop().untilMaxAttempts(5),
                Click.on(CARD_DROPDOWN)
        );
    }

    @Cuando("^el usuario selecciona Element Three$")
    public void elUsuarioSeleccionaElementThree() {
        pragma.attemptsTo(
                Click.on(ELEMENT_ONE_LIST),
                Click.on(ELEMENT_THREE_LIST)
        );
    }

    @Entonces("^el usuario deberia ver Element Three$")
    public void elUsuarioDeberiaVerElementThree() {
        pragma.should(
                seeThat(ObtenerTextoSegunCampoFlutter.presentaElMensaje(ELEMENT_THREE_LIST), equalTo("Element Three"))
        );
    }


}
