package org.example.stepdefinitions;

import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.example.questions.datapicker.DatePickerFactoryQuestion;
import org.example.tasks.datepicker.RedactarFecha;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static org.example.hooks.Hooks.pragma;
import static org.example.userinterfaces.pragma.PrincipalPage.CARD_DATEPICKER;
import static org.hamcrest.CoreMatchers.equalTo;

public class DatePickerScreenStepDefinitions {

    @Dado("^el usuario esta en Date Picker Screen$")
    public void elUsuarioEstaEnDatePickerScreen() {
       pragma.wasAbleTo(
               WaitUntil.the(CARD_DATEPICKER, isVisible()).forNoMoreThan(5).seconds(),
               Click.on(CARD_DATEPICKER)
       );
    }

    @Cuando("^el usuario redacta la fecha (.*) en el calendario$")
    public void elUsuarioRedactaFechaEnElCalendario(String fecha) {
        pragma.attemptsTo(
                RedactarFecha.enCampo(fecha)
        );
    }

    @Entonces("^el usuario deberia ver la fecha (.*)$")
    public void elUsuarioDeberiaVerLaFecha(String fechaEsperada){
        pragma.should(
                seeThat("la fecha ingresada", DatePickerFactoryQuestion.fechaIngresada(), equalTo(fechaEsperada) )
        );
    }
}
