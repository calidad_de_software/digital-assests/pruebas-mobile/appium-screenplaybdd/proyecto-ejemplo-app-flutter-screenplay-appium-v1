package org.example.stepdefinitions;

import co.com.devco.automation.mobile.actions.Scroll;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.example.questions.ObtenerTextoSegunCampoFlutter;
import org.example.tasks.formulario.DiligenciarFormulario;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static org.example.hooks.Hooks.pragma;
import static org.example.userinterfaces.pragma.FormularioPage.CARD_INPUT;
import static org.example.userinterfaces.pragma.FormularioPage.MENSAJE_FORMULARIO_VALIDO;
import static org.example.userinterfaces.pragma.PrincipalPage.CARD_BUTTONS_DEMO;
import static org.hamcrest.Matchers.equalTo;

public class FormImputsScreenStepDefinitions {

    @Dado("que el usuario esta en Inputs Screen")
    public void ingresarEnInputsScreen(){
        pragma.attemptsTo(
                WaitUntil.the(CARD_BUTTONS_DEMO, isVisible()).forNoMoreThan(5).seconds(),
                Scroll.untilVisibleTarget(CARD_INPUT).toBottom().untilMaxAttempts(5),
                Click.on(CARD_INPUT)
        );
    }

    @Cuando("^diligencia el formulario$")
    public void diligenciarFormulario() {
        pragma.attemptsTo(DiligenciarFormulario.deFlutter());
    }

    @Entonces("^el usuario deberia ver el mensaje (.*)$")
    public void verificaElMensajeDeSaludo(String mensaje) {
        pragma.should(seeThat(ObtenerTextoSegunCampoFlutter.presentaElMensaje(MENSAJE_FORMULARIO_VALIDO), equalTo(mensaje)));
    }
}
